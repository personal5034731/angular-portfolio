import { Component, HostBinding, OnInit, computed, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { ActivatedRoute, RouterOutlet } from '@angular/router';
import { HeaderComponent } from './shared/components/header/header.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, MatIconModule, HeaderComponent],
  template: `<router-outlet>
    <div class="relative">
        <div class="fixed top-0 left-0 right-0 z-10">
          <div class="w-full flex h-auto">
          <app-header class="flex flex-auto"></app-header>
          <div class="flex flex-auto transition duration-300 ease-in-out delay-50 bg-my-custom-light dark:bg-my-custom-dark justify-center items-center">
            <button (click)="setDarkMode()" class="bg-my-custom-dark dark:bg-my-custom-light rounded-full
            xl:h-12 xl:w-12
            lg:h-11 lg:w-11
            md:h-10 md:w-10
            sm:h-9 sm:w-9
            max-[640px]:h-8 max-[640px]:w-8
            max-[400px]:h-8 max-[400px]:w-8
            max-[300px]:h-8 max-[300px]:w-8">
              @if (!darkMode$()) {
                <span><mat-icon class="transform mt-1
                  xl:scale-110
                  lg:scale-100
                  md:scale-90
                  sm:scale-75
                  max-[640px]:scale-75
                  max-[400px]:scale-75
                  max-[300px]:scale-75 text-my-custom-light">brightness_7</mat-icon></span>
              }@else {
                <div><mat-icon class="transform mt-1 scale
                  xl:scale-110
                  lg:scale-100
                  md:scale-90
                  sm:scale-75
                  max-[640px]:scale-75
                  max-[400px]:scale-75
                  max-[300px]:scale-75 text-my-custom-dark">brightness_2</mat-icon></div>
              }
            </button>
            </div>
            </div>
        </div>
      </div>
</router-outlet>`
})
export class AppComponent implements OnInit {
  private darkMode = signal<boolean | any>(
    this.getItem
  )

  getItem() {
      this.darkMode.set(
        JSON.parse(window.localStorage.getItem('darkMode') ?? 'false')
      )
  }

  constructor(private activateRoute: ActivatedRoute) {
      window.localStorage.setItem('darkMode', JSON.stringify(this.darkMode()))
  }

  protected readonly darkMode$ = computed(() => this.darkMode());

  @HostBinding('class.dark') get mode() {
    return this.darkMode();
  }

  setDarkMode() {
    this.darkMode.set(!this.darkMode());
  }

  ngOnInit(): void {
      this.activateRoute.fragment.subscribe((section) => {
        this.goToSection(section);
      });
  }

  goToSection(section: any = ""): void {
      const element = document.getElementById(section);
      if (element) {
        element.scrollIntoView({ behavior: "smooth" });
      }
  }

}
