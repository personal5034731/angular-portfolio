import { Routes } from '@angular/router';

export const routes: Routes = [
  { path: 'portfolio', loadComponent: () => import('./modules/portfolio/portfolio.component').then(m => m.PortfolioComponent) },
  { path: '', redirectTo: '/portfolio', pathMatch: 'full' }
];
