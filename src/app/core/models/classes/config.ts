import { HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
class ConfigUrl{

  BaseUrl(): String{
    const localUrl: string = "http://localhost:8080/";
    const appUrl: string = import.meta.env.NG_APP_X_URL;
    return appUrl;
  }

  Header(): HttpHeaders{
    return new HttpHeaders().set('ngrok-skip-browser-warning', 'true');
  }

}

export { ConfigUrl }
