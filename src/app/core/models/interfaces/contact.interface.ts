interface RegisterContact {
  name: string;
  email: string;
  message: string;
}

export { RegisterContact };
