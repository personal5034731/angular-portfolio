interface Project {
  id?: number;
  title: string;
  description: string;
  image: string;
  origindate: string;
}

interface ProjectsResults {
  projects: Project[];
}

export {ProjectsResults, Project};
