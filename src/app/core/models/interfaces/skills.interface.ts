interface HardSkills {
  title: string;
  image?: string;
  percentage?: string;
  color: string;
}

interface HardSkillsResults {
  hardskills: HardSkills[];
}

export {HardSkillsResults, HardSkills};
