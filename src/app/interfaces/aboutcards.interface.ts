export interface AboutInterface{
  date: string,
  title: string,
  description: string
}
