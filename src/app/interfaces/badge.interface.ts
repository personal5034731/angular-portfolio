export interface BadgeInterface{
  title: string,
  percentage?: string,
  image?: string,
  gradient: string
}
