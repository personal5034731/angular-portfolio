import { Component } from '@angular/core';
import { SkillcardsComponent } from '../../shared/components/skillcards/skillcards.component';
import { AboutInterface } from '../../interfaces/aboutcards.interface';

@Component({
  selector: 'app-about',
  standalone: true,
  imports: [
    SkillcardsComponent,
  ],
  templateUrl: './about.component.html',
  styleUrl: './about.component.css'
})
export class AboutComponent {
  public DataExperience!: AboutInterface[]
  public DataEducation!: AboutInterface[]

  constructor(){
    this.setDataExperience()
    this.setDataEducation()
  }

  private setDataEducation(): void{
    this.DataEducation = [
      {
        date: '2020-2023',
        title: 'Bachelor Degree in Software Development',
        description: 'Degree obtained in the third year: Engineering Technician with Specialization in Software Development and honors chapter with an academic index of 2.77'
      },
      {
        date: '2018-2020',
        title: 'Bachelor Degree in Computer Networks',
        description: 'Studies carried out until second year.'
      },
      {
        date: '2016-2018',
        title: 'Degree in computer science',
        description: 'Bachelor degree in computer science at the Stella Sierra General Basic Educational Center.'
      }
    ]
  }

  private setDataExperience(): void{
    this.DataExperience = [
      {
        date: '2022',
        title: 'RAZ',
        description: 'In this oportunity I worked for the independent company Zwest Group as Developer Training, I solved many problems that the project had such as performance, we used 4 users roles, i did a crud for manage soccer tickets, the website is about Sports Manage with atheles, leaders, teams and more.'
      },
      {
        date: '2022',
        title: 'CriaderoBL',
        description: 'I worked for the independent company Zwest Group as Developer Training, i builded a slider brands and set emails automations using Mailgun service. In this project also I used Laravel Mix with Vue.JS, it was a great proyect.'
      },
      {
        date: '2020',
        title: 'Freelance',
        description: 'I had the opportunity to make a desktop console software for 2 clients using C++ Language in Turbo C and Java for a University Project, I made a Soda Machine.'
      }
    ]
  }
}
