import { Component, inject } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { RouterLink } from '@angular/router';
import { ContactService } from './services/contact.service';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { RippleModule } from 'primeng/ripple';
import { ButtonModule } from 'primeng/button';
import { ProgressSpinnerModule } from 'primeng/progressspinner';

@Component({
  selector: 'app-contact',
  standalone: true,
  imports: [
    FormsModule,
    RouterLink,
    ReactiveFormsModule,
    ToastModule,
    ProgressSpinnerModule,
    RippleModule,
    ButtonModule
],
  templateUrl: './contact.component.html',
  styleUrl: './contact.component.css',
  providers:  [MessageService],
})
export class ContactComponent {

  private fb = inject(FormBuilder)
  private contactService = inject(ContactService)
  private messageService = inject(MessageService)
  loadingmailer: Boolean = true;
  loaderStatus: Boolean = false;
  contactForm!: FormGroup;

  ngOnInit() {
    this.contactForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(6)]],
      email: ['',  [Validators.required, Validators.minLength(6), Validators.email]],
      message: ['', [Validators.required, Validators.minLength(1)]]
    });
  }

  sendMail() {
    this.loadingmailer = false;
    this.loaderStatus = true;
    this.contactService.EmailSender(this.contactForm.value)
      .subscribe(
        {
          next: () => {
            setTimeout(() => {
              this.loaderStatus = false;
              this.showSuccess();
              this.loadingmailer = true;
            }, 1000);
          },
          error: (e) => {
            alert("Error en el envio de email: "+e);
          },
        });
  }

  showError() {
    this.messageService.clear();
    this.messageService.add({ key: 'register error', severity: 'error', summary: 'Error', detail: 'Ocurrió un error.. reintentelo' });
  }

  showSuccess() {
    this.messageService.clear();
    this.messageService.add({ key: 'register sucess', severity: 'success', summary: 'Éxito', detail: 'La operación se completó correctamente.' });
  }

}
