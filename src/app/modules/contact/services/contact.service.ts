import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { ConfigUrl } from '../../../core/models/classes/config';
import { Observable } from 'rxjs';
import { RegisterContact } from '../../../core/models/interfaces/contact.interface';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  private http = inject(HttpClient);
  private configUrl = inject(ConfigUrl);

  EmailSender(registerProject: RegisterContact): Observable<RegisterContact>{
    const headers = this.configUrl.Header();
    return this.http.post<RegisterContact>(this.configUrl.BaseUrl()+'psmail/send', registerProject, { headers });
  }
}
