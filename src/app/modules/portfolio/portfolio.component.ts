import { Component } from '@angular/core';
import { HomeComponent } from '../home/home.component';
import { ProjectsComponent } from '../projects/pages/projects.component';
import { ShapeSectionComponent } from '../../shared/components/shape-section/shape-section.component';
import { AboutComponent } from '../about/about.component';
import { SkillsComponent } from '../skills/pages/skills.component';
import { ContactComponent } from '../contact/contact.component';

@Component({
  selector: 'app-portfolio',
  standalone: true,
  imports: [
    HomeComponent,
    ProjectsComponent,
    AboutComponent,
    ShapeSectionComponent,
    SkillsComponent,
    ContactComponent
  ],
  templateUrl: './portfolio.component.html',
  styleUrl: './portfolio.component.css'
})
export class PortfolioComponent {

}
