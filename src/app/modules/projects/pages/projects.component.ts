import { Component, inject } from '@angular/core';
import { AsyncPipe, NgOptimizedImage } from '@angular/common'
import { ProjectsService } from '../services/projects.service';
import { Project } from '../../../core/models/interfaces/projects.interface';
import { HttpClientModule} from '@angular/common/http';
import { ConfigUrl } from '../../../core/models/classes/config';
import { ErrorpageComponent } from '../../../shared/components/errorpage/errorpage.component';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-projects',
  standalone: true,
  imports: [
    NgOptimizedImage,
    HttpClientModule,
    ErrorpageComponent,
    AsyncPipe
  ],
  providers: [
    ProjectsService,
    ConfigUrl
  ],
  templateUrl: './projects.component.html',
  styleUrl: './projects.component.css',
  schemas: []
})
export class ProjectsComponent{
  public projectFetchff!: Project[]

  projectService = inject(ProjectsService)
  projectFetch$: Observable<Project[]> = this.projectService.GetAllProject();

  ngOnInit(): void{
    /*
    this.projectService.GetAllProject().subscribe(
      {
        next: (res: Project[]) => {
            res['forEach']((i: {title: any; description: any; image: any; origindate: any;}) => {
              this.projectFetch.push({
                title: i.title,
                description: i.description,
                image: i.image,
                origindate: i.origindate
              });
            });
            this.projectFetch = [...this.projectFetch]
        },
        error: (err) => {
          console.log(err);
        },
      }
    )
    /*
      this.dashboardService.GetAllInstruments()
      .subscribe(
        {
          next: (res) => {
            if (res.status === 404) {
              console.log('ocurrio un error para traer el softwarename')
            } else {
              this.datos = [];
                res['forEach']((item: { softwarename: any; version: any; softwaretype: any; username: any; }) => {
                  this.datos.push({
                    nombre: item.softwarename,
                    version: item.version,
                    tipo: item.softwaretype,
                    evaluador: item.username
                  });
                });
              this.datos = [...this.datos]
            }
          },
          error: (err) => {
            //console.log(err);
          },
        }
      )*/

    }
}
