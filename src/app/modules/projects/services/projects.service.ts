import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigUrl } from '../../../core/models/classes/config';
import { Project } from '../../../core/models/interfaces/projects.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  private configUrl: ConfigUrl

  constructor(private http: HttpClient,c: ConfigUrl) {
    this.configUrl = c
  }

  GetAllProject(): Observable<Project[]> {
    const headers = this.configUrl.Header();
    return this.http.get<Project[]>(this.configUrl.BaseUrl()+"project/all", { headers });
  }
}
