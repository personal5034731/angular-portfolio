import { Component, inject } from '@angular/core';
import { AsyncPipe, NgOptimizedImage, NgStyle } from '@angular/common'
import { BadgesComponent } from '../../../shared/components/badges/badges.component';
import { EMPTY, Observable, catchError, interval, of } from 'rxjs';
import { HardSkills } from '../../../core/models/interfaces/skills.interface';
import { ConfigUrl } from '../../../core/models/classes/config';
import { SkillsService } from '../services/skills.service';
import { HttpClientModule } from '@angular/common/http';
import { ErrorpageComponent } from '../../../shared/components/errorpage/errorpage.component';

@Component({
  selector: 'app-skills',
  standalone: true,
  imports: [
    NgOptimizedImage,
    BadgesComponent,
    AsyncPipe,
    HttpClientModule,
    NgStyle,
    ErrorpageComponent
  ],
  providers: [
    SkillsService,
    ConfigUrl
  ],
  templateUrl: './skills.component.html',
  styleUrl: './skills.component.css',
  schemas: []
})
export class SkillsComponent {
  hardSkillService = inject(SkillsService)
  hardskillFetch$: Observable<HardSkills[]> = this.hardSkillService.GetAllHardSkills();

  softSkillService = inject(SkillsService)
  softskillFetch$: Observable<HardSkills[]> = this.hardSkillService.GetAllSoftSkills();

  constructor(){}

  ngOnInit(): void{}

}
