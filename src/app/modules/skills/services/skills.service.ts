import { Injectable } from '@angular/core';
import { ConfigUrl } from '../../../core/models/classes/config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HardSkills } from '../../../core/models/interfaces/skills.interface';

@Injectable({
  providedIn: 'root'
})
export class SkillsService {

  private configUrl: ConfigUrl

  constructor(private http: HttpClient,c: ConfigUrl) {
    this.configUrl = c
  }

  GetAllHardSkills(): Observable<HardSkills[]>{
    const headers = this.configUrl.Header();
    return this.http.get<HardSkills[]>(this.configUrl.BaseUrl()+'skill/hard/all', {headers});
  }

  GetAllSoftSkills(): Observable<HardSkills[]>{
    const headers = this.configUrl.Header();
    return this.http.get<HardSkills[]>(this.configUrl.BaseUrl()+'skill/soft/all', {headers});
  }

}
