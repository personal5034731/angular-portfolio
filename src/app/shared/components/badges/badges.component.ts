import { Component, Input } from '@angular/core';
import {NgStyle} from '@angular/common';

@Component({
  selector: 'app-badges',
  standalone: true,
  imports: [
    NgStyle
  ],
  template: `@if (mode === "hard") {
          <div class="flex justify-between w-max rounded-full shadow-xl shadow-zinc-800
                          bg-gradient-to-r
                          xl:h-12
                          lg:h-11
                          md:h-10
                          sm:h-9
                          max-[640px]:h-8 max-[640px]:shadow-xl max-[640px]:shadow-zinc-800
                          max-[400px]:h-7 max-[400px]:shadow-xl max-[400px]:shadow-zinc-800
                          max-[300px]:h-6 max-[300px]:shadow-xl max-[300px]:shadow-zinc-800"
                          [ngStyle]="{background: gradient}">
            <div>
              <img class="rounded-full object-cover bg-white
                          xl:w-12 xl:h-12
                          lg:w-11 lg:h-11
                          md:w-10 md:h-10
                          sm:w-9 sm:h-9
                          max-[640px]:w-8 max-[640px]:h-8
                          max-[400px]:w-7 max-[400px]:h-7
                          max-[300px]:w-6 max-[300px]:h-6" src="{{img}}">
            </div>
            <div class="title text-xl flex items-center
            justify-center text-white text-shadow-lg shadow-blue-600 font-black mx-3
            xl:text-xl xl:mx-3
            lg:text-lg lg:mx-3
            md:text-base md:mx-3
            sm:text-sm sm:mx-3
            max-[640px]:text-[12px] max-[640px]:mx-2
            max-[400px]:text-[10px] max-[400px]:mx-2
            max-[300px]:text-[8px] max-[300px]:mx-2">
              {{lang}}
            </div>
            <div class="title flex items-center
            justify-center h-12 border-l-4
            border-zinc-700 text-white font-black
            xl:text-xl xl:w-12 xl:h-12
            lg:text-lg lg:w-12 lg:h-11
            md:text-base md:w-11 md:h-10
            sm:text-sm sm:w-11 sm:h-9
            max-[640px]:text-[12px] max-[640px]:w-10 max-[640px]:h-8
            max-[400px]:text-[10px] max-[400px]:w-8 max-[400px]:h-7
            max-[300px]:text-[8px] max-[300px]:w-8 max-[300px]:h-6">
              {{percent}}%
            </div>
          </div>
        }@else if (mode === "soft") {
          <div class="flex justify-between w-max rounded-full shadow-xl shadow-zinc-800
                          bg-gradient-to-r
                          xl:h-12
                          lg:h-11
                          md:h-10
                          sm:h-9
                          max-[640px]:h-8 max-[640px]:shadow-xl max-[640px]:shadow-zinc-800
                          max-[400px]:h-7 max-[400px]:shadow-xl max-[400px]:shadow-zinc-800
                          max-[300px]:h-6 max-[300px]:shadow-xl max-[300px]:shadow-zinc-800"
                          [ngStyle]="{background: gradient}">
            <div class="title text-xl flex items-center
            justify-center text-zinc-100 font-black mx-3
            xl:text-xl xl:mx-3
            lg:text-lg lg:mx-3
            md:text-base md:mx-3
            sm:text-sm sm:mx-3
            max-[640px]:text-[12px] max-[640px]:mx-2
            max-[400px]:text-[10px] max-[400px]:mx-2
            max-[300px]:text-[8px] max-[300px]:mx-2">
              {{lang}}
            </div>
          </div>
        }`
})
export class BadgesComponent {

  public color1:string = "#fff"
  @Input({
    alias: 'image'
  }) img?:string = ""

  @Input({
    alias: 'title',
    required: true
  }) lang:string = ""

  @Input({
    alias: 'percentage'
  }) percent?:string = ""

  @Input({
    alias: 'gradient',
    required: true
  }) gradient:string = ""

  @Input({
    alias: 'mode',
    required: true
  }) mode:string = ""
}
