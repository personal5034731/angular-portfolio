import { Component } from '@angular/core';
import { RouterLink } from '@angular/router';
import { LinkInterface } from '../../../interfaces/header.interface';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './header.component.html'
})
export class HeaderComponent{
  public Links!: LinkInterface[]

  constructor(){
    this.setLinks()
  }

  private setLinks(): void{
    this.Links = [
      {
        name: 'Home',
        route: '/portfolio'
      },
      {
        name: 'Projects',
        route: '/portfolio'
      },
      {
        name: 'About',
        route: '/portfolio'
      },
      {
        name: 'Skills',
        route: '/portfolio'
      },
      {
        name: 'Contact',
        route: '/portfolio'
      },
    ]
  }

}
