import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShapeSectionComponent } from './shape-section.component';

describe('ShapeSectionComponent', () => {
  let component: ShapeSectionComponent;
  let fixture: ComponentFixture<ShapeSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ShapeSectionComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ShapeSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
