import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-shape-section',
  standalone: true,
  imports: [],
  templateUrl: './shape-section.component.html',
  styleUrl: './shape-section.component.css'
})
export class ShapeSectionComponent {
  @Input() typeshape!: string
}
