import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-skillcards',
  standalone: true,
  imports: [],
  template: `<div class="flex">
  <div class="circle"></div>
  <div class="timelime basis-1/12"></div>
  <div class="flex justify-left transition duration-300 ease-in-out delay-50 bg-my-custom-dark dark:bg-my-custom-light rounded-lg my-4 basis-11/12
  xl:mx-20
  lg:mx-20
  md:mx-10
  sm:mx-10
  max-[640px]:mx-2
  max-[400px]:mx-2
  max-[300px]:mx-2">
    <div class="overflow-auto h-44 m-2">
      <div class="title transition duration-300 ease-in-out delay-50 text-blue-400 dark:text-blue-800 font-semibold
      xl:text-xl
      lg:text-lg
      md:text-base
      sm:text-sm
      max-[640px]:text-[11px]
      max-[400px]:text-[11px]
      max-[300px]:text-[11px]">{{date}}</div>
      <div class="font-semibold mb-2 transition duration-300 ease-in-out delay-50 text-my-custom-light dark:text-my-custom-dark
      xl:text-base
      lg:text-sm
      md:text-xs
      sm:text-[10px]
      max-[640px]:text-[10px]
      max-[400px]:text-[10px]
      max-[300px]:text-[10px]">{{title}}</div>
      <p class="font-extralight transition duration-300 ease-in-out delay-50 text-my-custom-light dark:text-my-custom-dark
      xl:h-[4rem] xl:text-base
      lg:h-[3rem] lg:text-sm
      md:h-[2rem] md:text-xs
      sm:h-[1rem] sm:text-[10px]
      max-[640px]:h-[8px] max-[640px]:text-[10px]
      max-[400px]:h-[4px] max-[400px]:text-[10px]
      max-[300px]:h-[2px] max-[300px]:text-[10px]">{{description}}</p>
    </div>
  </div>
</div>
`,
  styles: `
    .timelime{
      border-left: 4px solid rgb(59, 130, 246);
      margin-left: 8px;
    }

    .circle{
      width: 20px;
      height: 20px;
      background-color: rgb(59, 130, 246);
      border-radius: 50%;
      position: absolute;
    }`
})
export class SkillcardsComponent {
  @Input({required: true, alias: 'date'}) date:string = ""
  @Input({required: true, alias: 'title'}) title:string = ""
  @Input({required: true, alias: 'description'}) description:string = ""
}
