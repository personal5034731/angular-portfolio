/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
      colors: {
        'my-custom-dark': 'var(--my-custom-dark)',
        'my-custom-light': 'var(--my-custom-light)',
        'my-shape-section-color': 'var(--my-shape-section-color)',
      },
    },
  },
  plugins: [
    require('tailwindcss-animated')
  ],
}
